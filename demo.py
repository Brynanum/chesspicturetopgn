from dataToPGN import DataToPGN
from readPicture import ReadPicture

# ==== Extract PGN data from the picture
picture=ReadPicture('data/1.png')
DataToPGN('1',picture.title,picture.data) # PGN result in data/1.pgn
print('=======================================================')

# ==== Extracted untreated text only from the picture (without treatment)
print(ReadPicture.pictureToText('data/8.jpg'))
print('=======================================================')
# ==== Extracted text with only a built dictionnary treatment on data
picture=ReadPicture('data/8.jpg')
print('Title : '+picture.title)
print('Data :\n'+picture.data)
print('=======================================================')
# ==== Extracted text with dictionnary + game simulation + title treatments
DataToPGN('8',picture.title,picture.data)
with open('data/8.pgn','r') as file:
	print(file.read())
