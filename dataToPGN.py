# -*- coding: utf-8 -*-
import os.path
import chess
import re

class DataToPGN:
	"""
		Write a PGN file from data

		PROPERTIES
		annotations Dictionnary(String, Int) : Pair of annotation symbol with its NAG number
		figurines Dictionnary(String, String) : Pair of figurines symbol with is english letter
		result (List) : List all possible result score
		files (List): List of all files of a chessboard
		ranks (List): List of all ranks of a chessboard
	"""

	annotations = {"!":1,
				"?":2,
				"!!":3,
				"??":4,
				"!?":5,
				"?!":6,
				"□":7,
				"=":10,
				"∞":13,
				"+/=":14,
				"=/+":15, 
				"+/-":16,
				"-/+":17, 
				"+-":18,
				"-+":19,
				"=/∞<":[42,43],
				"=/∞":[44,45],
				"=/∞>":[46,47],
				"⨀":[22,23],
				"∆":140,
				"⌓":142}
	   
	figurines = {"\u2654":"K", "♚":"K", 
				"\u2655":"Q", "♛":"Q",
				"\u2656":"R", "♜":"R",
				"\u2657":"B", "♝":"B",
				"\u2658":"N", "♞":"N"}

	results = ["1/2-1/2", "½-½", "1-0", "0-1", "*"]
			  
	files = ["a", "b", "c", "d", "e", "f", "g", "h"]
	
	ranks = [str(k) for k in range(1,9)]
			  
	def annotationToNAG(self, annotation, turn):
		"""
			Convert annotation symbol to its annotation NAG.
			annotation (String) : annotation symbol
			turn (Int) : Which color is playing
			RETURN String
		"""
		if symbol in self.annotations:
			return("$" + str(self.annotations[annotation][turn]))
 
	def createPGN(self, file):
		"""
			Create a PGN file with empty header
			file (String) : file path			
			RETURN Boolean Is file editable
		"""
		if os.path.isfile(file):
			v = input('Are you sure to overwrite ' + file[5:] + '? (y/n) \n')
			if v=='n':
				return False
		return True

	def isMove(self, s):
		"""
			Check if string is a chess move
			s (String) : part of game data
			RETURN Boolean
		"""
		for l in s:
			if l not in self.figurines and l not in self.files and l not in self.ranks and l not in self.annotations and l not in self.results and l!='O' and l!='-' and l!='x' and l!='=':
				return False
		return True

	def toPGN(self, s, b):
		"""
			Convert string to PGN format
			s (String) : part of game data
			b (String) : board associated with the game
		"""
		o = ""
		for l in s:
			if l in self.figurines:
				o += self.figurines[l]
			elif l in self.annotations:
				a = -1
				if b.turn == chess.BLACK:
					a = 0
				o += self.annotationToNAG(l, a)
			else:
				o += l
		return o

	def createPGNHead(self,info,data):
		"""
			Get information to put in head with PGN format
			info (String) : some information
			data (String) : the game
			RETURN a dictionnary with Event / Site / Date / Round / White / Black / Result
		"""
		# ==== Initialization
		title={'Event':False,'Site':False,'Date':False,'Round':False,'White':False,'Black':False,'Result':False}
		# ==== Result
		for end in ["1/2-1/2", "½-½" "1-0", "0-1", "*"]:
			if end in data:
				title['Result']=end
				break
		if not title['Result'] and ('esult' in info):
			index=info.index('esult')+7
			title['Result']=info[index:index+info[index:].index('"')]
		# ==== Date
		datoom=re.search('[12][0-9]{3}\.[0-9]{2}\.[0-9]{2}',info)
		if datoom:
			title['Date']=datoom.group(0)
		else:
			datoom=re.search('[12][0-9]{3}',info)
			if datoom:
				title['Date']=datoom.group(0)+'.??.??'
		# ==== Site
		datoom=re.search('[a-z0-9]+\.(com|org)',info)
		if datoom:
			title['Site']=datoom.group(0)
		# ==== White
		datoom=re.search('\w+(| ),(| )\w+',info)
		if datoom:
			title['White']=datoom.group(0)
		# ==== Black
		if(title['White']):
			datoom=re.search('\w+(| ),(| )\w+',info[info.index(title['White'])+len(title['White']):])
			if datoom:
				title['Black']=datoom.group(0)
		# ==== Event
		datoom=re.search('[\w -]+[12][0-9]{3}',info)
		if datoom:
			title['Event']=datoom.group(0)
		# ==== Round
		datoom=re.search('ound "[0-9]"',info)
		if datoom:
			title['Round']=datoom.group(0)[6:-1]
		# ==== Return
		for key in title:
			if not title[key]:
				title[key]=''
		return title

	def __init__(self, name, info, data):
		"""
			Write PGN file from info and data
			name (String) : filename
			info (String) : header information (eg: 'Moskva 1983')
			data (String) : game data (eg '1.e4 e5 2.♘f3 ♘c6')
		"""
		file = 'data/'+str(name)+'.pgn'
		if self.createPGN(file):
			f = open(file, 'w', encoding='utf-8')
			head = self.createPGNHead(info,data)
			for key in head:
				f.write('['+key+' "'+head[key]+'"]\n')
			i = 0
			b = chess.Board()
			d = re.split('[. ]',data)
			for w in d:
				if w.isdigit():
					f.write(w+'.')
				elif self.isMove(w):
					w = self.toPGN(w, b)
					f.write(w+' ')
					# Interested function : b.parse_san(w) in b.legal_moves, b.push_san(w)
			f.close()
