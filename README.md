# Picture to PGN
![Illustration of the project](https://gitlab.com/Brynanum/chesspicturetopgn/raw/master/pictures/3.png)

## Introduction
This project permit to convert a picture to PGN format, like pictures in data folder.

## How can I use it ?
According to your wanted result, there are many methods. All of these is shown in demo.py : execute it for seeing possibilities.

## How can I improve the quality of the extracted data ?
Follow these tips on your picture to get the best result :
* Maximize the constrast between characters and background
* Binarize it (render it in black and white)
* Crop useless areas (or hide it with colored squares)
* Realize it in a lightning environment (under the sun, or by a scanner)
* Keep a flat support
You are able to realize these experiments with pictures 10 and more in data folder.

## Documentation
You can find all technical documentation in the "doc" folder, as HTML files.
You are able to see directly these files on your web browser : copy the link of the wanted documentation file on https://htmlpreview.github.io to see it.

## Required external Python libraries
* [TesserOCR](https://github.com/sirfz/tesserocr)
* [Re](https://docs.python.org/3/library/re.html)
* [Python-chess](https://github.com/niklasf/python-chess)

## Organisation and features
You can see it in CHANGELOG.md file.

## Contributors
* Clément GOBÉ (alias @gobec)
* Bryan FAUQUEMBERGUE (alias @Brynanum)
