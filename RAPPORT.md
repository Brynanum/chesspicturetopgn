# Rapport
## Description du sujet
Le but est d'extraire le plus fidèlement possible une description littérale d'une partie d'échec en image, pour ensuite la restituer au format PGN et sous forme textuelle.
## Description de la réalisation
Pour cela, plusieurs algorithmes viennent jouer leur rôle :
* Une extraction textuelle brute depuis l'image
* Un dictionnaire de remplacement d'ensemble de caractères (notamment pour identifier les symboles de pion)
* Un simulateur de jeu d'échec, pour vérifier les coups (initié mais non terminé)
* Un convertisseur sous format PGN avec en-têtes
## Améliorations possibles
Le dictionnaire possède déjà plusieurs cas rectifiant la majorité des caractères. Cependant, il est intéressant de le tester sur les images que vous avez, et de le compléter : plus il sera complet, plus il couvrira la majorité des symboles.
De plus, la simulation du jeu d'échec peut être grandement amélioré : cela peut permettre de certifier de manière certaine l'extraction des données, et d'apporter d'autres corrections encore.
## Approche de la réalisation
Nous nous sommes partagés le travail : Bryan Fauquembergue (@Brynanum) s'est principalement occupé du dictionnaire et de l'extraction textuelle (ainsi qu'une petite étude sur la qualité des données extraites), tandis que Clément Gobé (@gobec) s'est penché sur le simulateur et le convertisseur. Nous travaillons chacun sur une branche respective, laissant la branche _master_ pour ce qui peut être mis en publique.
## Apprentissages
### Bryan Fauquembergue (@Brynanum)
Le niveau informatique n'a pas été un problème pour moi, étant à l'aise avec ce langage. Néanmoins, même si ce qui est demandé est purement informatique, une petite piqûre de rappel sur les deux points les plus importants dans un projet ne m'a pas fait de mal :
* La communication. Même si le client n'est pas forcément un informaticien et peut ne pas avoir conscience de certaines choses, il reste la personne à satisfaire à la fin du projet. Rien n'empêche le développeur de conseiller ou d'aiguiller, mais seul le client fixe le point d'arrivée.
* L'environnement du projet. Le développeur comme le client peut ne pas avoir tous les moyens en sa possession : pour le développeur on parle souvent de la version des outils utilisés, pour le client c'est la façon dont il peut informer et communiquer (tel que la condition de prise des photos ici). Cependant, le simple fait d'aménager son environnement peut permettre de faciliter bien des choses, sans même toucher à un ordinateur.
### Clément Gobé (@gobec)
Le projet m'a appris à gérer la validité des données à écrire dans un fichier. De plus, ce projet m'a également apporté des connaissances dans un autre domaine que l'informatique, avec les recherches que j'ai effectués sur la notations au format PGN.