# Story of the project

## March 19th, 2019
* Discover and analyze instructions (Bryan + Clement)
* Change characters for a PGN format (Clement)
  * Realize translation on paper (DONE)
  * Implement it (NOT FINISHED)
* Extract characters from pictures (Bryan)
  * Extract uncleaned data (DONE)
  * Realize a dictionnary for translating (NOT FINISHED)

## March 26th, 2019
* Extract characters from pictures (Bryan)
  * Realize a dictionnary for translating (DONE)
  * Integrate acceptation of special characters (DONE)
  * Documentation + arrange code in classes (DONE)
* Change characters for a PGN format (Clement)
  * Implement it (DONE)
  * Extract PGN and others information into a external file (DONE)
* Simulate a board game for verifying data and to correct it (Clement) (NOT FINISHED)
  * Create a board game and exploit it (DONE)

## May 15th, 2019
* Improve the quality of extracted data (Bryan)
  * Add some pictures as examples (DONE)
  * Find tips to improve extracted data only with the content of the picture (DONE)
  * Consider pictures with pawns as letter in program (DONE)
* Simulate a board game for verifying data and to correct it (Clement) (NOT FINISHED)
  * Verify word format (DONE)
  * Verify if chess move is legal (NOT FINISHED)
  * Consider played turn (e4,f5...) and pawns (K,N...) (NOT FINISHED)

## May 16th, 2019
* Improve the quality of data (Bryan)
  * Realize an automatic PGN header (DONE)
* Study results according to the started picture and the situation (Bryan, DONE)
* Update presentation and information on GitLab (Bryan, DONE)
* Simulate a board game for verifying data and to correct it (Clement)
  * Verify if chess move is legal (A PART DONE AND USEFULL, NOT FINISHED)
  * Consider played turn (e4,f5...) and pawns (K,N...) (DONE)
  * Update comments and documentation (DONE)
  * Consider more annotations (NOT FINISHED)
